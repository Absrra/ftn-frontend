import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiHelper {

  private static _instance: ApiHelper;
  public api_base_url = 'http://147.135.228.122:8080/';

  constructor() {
    if ( !environment.production ) {
        this.api_base_url = 'http://127.0.0.1:1337/';
    }
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

}
