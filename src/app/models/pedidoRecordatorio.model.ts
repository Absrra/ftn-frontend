
export interface IPedidorecordatorio {
    id?: string;
    descripticion?: string;
    owner?: string;
}

export class Pedidorecordatorio {
    id: string;
    descripticion: string;
    owner: string;
}
