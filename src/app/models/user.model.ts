import { ICompany } from './company.model';


export interface IUser {
    id?: string;
    rut?: string;
    company_id?: string;
    first_name?: string;
    last_name?: string;
    username?: string;
    phone?: string;
    type_id?: number;
    status?: boolean;
    email?: string;
    password?: string;
    lang?: string;
    created?: Date;
    updated?: Date;
}

export class User {
    id: string;
    rut: string;
    company_id: string;
    first_name: string;
    last_name: string;
    username: string;
    phone: string;
    type_id: number;
    status: boolean;
    email: string;
    password?: string;
    lang: string;
    created: Date;
    updated: Date;
}
