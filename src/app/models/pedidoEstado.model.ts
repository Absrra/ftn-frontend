
export interface IPedidoestado {
    id?: string;
    name?: string;
    status?: boolean;
}

export class Pedidoestado {
    id: string;
    name: string;
    status: boolean;
}
