
export interface IMunicipio {
    id?: string;
    id_departamento?: string;
    nombre?: string;
}

export class Municipio {
    id: string;
    id_departamento: string;
    nombre: string;
}
