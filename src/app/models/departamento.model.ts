
export interface IDepartamento {
    id?: string;
    nombre?: string;
}

export class Departamento {
    id: string;
    nombre: string;
}
