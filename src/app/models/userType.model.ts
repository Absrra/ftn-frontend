
export interface IUserType {
    name?: string;
    status?: string;
}

export class UserType {
    name: string;
    status: string;
}
