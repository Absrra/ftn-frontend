
export interface IPedidoseguimiento {
    id?: string;
    descripcion?: string;
    fecha_prox?: Date;
}

export class Pedidoseguimiento {
    id: string;
    descripcion: string;
    fecha_prox: Date;
}
