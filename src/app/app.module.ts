import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdProgressSpinnerModule } from '@angular/material';
import { MdSnackBarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PanelAdminComponent } from './components/panel-admin/panel-admin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OrdersComponent } from './components/orders/orders.component';
import { BillsComponent } from './components/bills/bills.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductsCategoryComponent } from './components/products-category/products-category.component';
import { ProductsCatalogsComponent } from './components/products-catalogs/products-catalogs.component';
import { AlertsComponent } from './components/alerts/alerts.component';
import { AlertsSaleComponent } from './components/alerts-sale/alerts-sale.component';
import { PromotionalCodesComponent } from './components/promotional-codes/promotional-codes.component';
import { TemplateComponent } from './components/template/template.component';
import { PointSystemComponent } from './components/point-system/point-system.component';
import { RegionalConfigurationComponent } from './components/regional-configuration/regional-configuration.component';
import { CompanyConfigurationComponent } from './components/company-configuration/company-configuration.component';
import { ConfigurationComponent } from './components/configuration/configuration.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ProfileComponent } from './components/profile/profile.component';

import 'hammerjs';




export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    PanelAdminComponent,
    DashboardComponent,
    OrdersComponent,
    BillsComponent,
    ClientsComponent,
    ProductsComponent,
    ProductsCategoryComponent,
    ProductsCatalogsComponent,
    AlertsComponent,
    AlertsSaleComponent,
    PromotionalCodesComponent,
    TemplateComponent,
    PointSystemComponent,
    RegionalConfigurationComponent,
    CompanyConfigurationComponent,
    ConfigurationComponent,
    LoginComponent,
    UsersComponent,
    ResetPasswordComponent,
    ProfileComponent,
    ChangePasswordComponent
  ],
  imports: [
    HttpClientModule,
    HttpModule,
    MdSnackBarModule,
    MdProgressSpinnerModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    MyDateRangePickerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
