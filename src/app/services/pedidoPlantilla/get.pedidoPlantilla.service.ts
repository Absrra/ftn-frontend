import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IPedidoplantilla } from '../../models/pedidoPlantilla.model';


@Injectable()
export class GetPedidoPlantillaService extends StorageHelper {
    private pedido_id: string;

    constructor(private http: Http) { super(); }

    public init(pedido_id_: string): GetPedidoPlantillaService {
        this.pedido_id = pedido_id_;
        return this;
    }

    public getData(): Observable<IPedidoplantilla> {

        const service_url = ApiHelper.Instance.api_base_url + `user/` + this.pedido_id;
        const ctx   =   this.getContext();

        const _headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + ctx.token
        });

        const _options = new RequestOptions({ headers: _headers });

        return this.http.get(service_url, _options)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));

    }
}
