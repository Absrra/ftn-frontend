import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IPedidoalerta } from '../../models/pedidoAlerta.model';

@Injectable()
export class UpdatePedidoAlertaService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private pedido_id: string;
  private pedido_data: IPedidoalerta;

  public init( pedido_id_: string, pedido_data_: IPedidoalerta ): UpdatePedidoAlertaService {
    this.pedido_id = pedido_id_;
    this.pedido_data = pedido_data_;
    return this;
  }


  public updatePedido(): Observable <IPedidoalerta> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.pedido_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.pedido_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
