import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { ICompany } from '../../models/company.model';

@Injectable()

export class AddCompanyService extends StorageHelper {

    private new_user: ICompany;

    constructor( private http: Http ) { super(); }

    public init( new_user_: ICompany ): AddCompanyService {

        this.new_user = new_user_;
        return this;
    }


    public createUser(): Observable <ICompany> {


        const service_url = ApiHelper.Instance.api_base_url + 'company';
        const ctx   =   this.getContext();


        const _headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + ctx.token
        });

        const _options = new RequestOptions( { headers: _headers } );

        const _params = this.new_user;

        return this.http.post( service_url, _params, _options)
            .map((res: Response) => res.json() )
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));

    }

}
