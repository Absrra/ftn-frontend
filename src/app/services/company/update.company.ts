import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { ICompany } from '../../models/company.model';

@Injectable()
export class UpdateCompanyService extends StorageHelper {

  constructor( private http: Http ) {super(); }

  private company_id: string;
  private company_data: ICompany;

  public init( company_id_: string, company_data_: ICompany ): UpdateCompanyService {
    this.company_id = company_id_;
    this.company_data = company_data_;
    return this;
  }


  public update(): Observable <ICompany> {

    const service_url = ApiHelper.Instance.api_base_url + `company/${this.company_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.company_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
