import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { ICompany } from '../../models/company.model';


@Injectable()
export class GetCompanyService extends StorageHelper {
    private company_id: string;

    constructor(private http: Http) { super(); }

    public init(company_id_: string): GetCompanyService {
        this.company_id = company_id_;
        return this;
    }

    public getData(): Observable<ICompany> {

        const service_url = ApiHelper.Instance.api_base_url + `company/` + this.company_id;
        const ctx   =   this.getContext();


        const _headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + ctx.token
        });

        const _options = new RequestOptions({ headers: _headers });

        return this.http.get(service_url, _options)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));

    }
}
