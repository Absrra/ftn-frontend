import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ApiHelper } from '../../helpers/api.helper';
import { IContext } from '../../models/context.model';

@Injectable()
export class LoginAuthService {

  public email: string;
  public password: string;

  constructor(private http: Http) { }

  /**
   * init
   */
  public init(email_: string, password_: string) {
    this.email  = email_;
    this.password = password_;
    return this;
  }


  /**
   * prueba
   */
  public login(): Observable<any> {

    const service_url = ApiHelper.Instance.api_base_url + 'auth';
    // const service_url = 'http://147.135.228.122:8080/Auth/login?email=' + this.email + '&password=' + this.password;

    const params = {
      email : this.email,
      password : this.password
    };

    const headers = new Headers({
        'Content-Type': 'application/json'
    });

    const options = new RequestOptions( { headers: headers } );



    return this.http.post(service_url, params, options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

}
