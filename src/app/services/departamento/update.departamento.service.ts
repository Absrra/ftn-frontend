import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IDepartamento } from '../../models/departamento.model';

@Injectable()
export class UpdateDepartamentoService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private departamento_id: string;
  private departamento_data: IDepartamento;

  public init( departamento_id_: string, departamento_data_: IDepartamento ): UpdateDepartamentoService {
    this.departamento_id = departamento_id_;
    this.departamento_data = departamento_data_;
    return this;
  }


  public updateDepartamento(): Observable <IDepartamento> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.departamento_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.departamento_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
