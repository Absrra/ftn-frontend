import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IOpcionesfacturas } from '../../models/opcionesFactura.model';

@Injectable()
export class UpdateOpcionesFacturasService extends StorageHelper {

  constructor( private http: Http ) { super(); }

  private opcion_id: string;
  private opcion_data: IOpcionesfacturas;

  public init( opcion_id_: string, opcion_data_: IOpcionesfacturas ): UpdateOpcionesFacturasService {
    this.opcion_id = opcion_id_;
    this.opcion_data = opcion_data_;
    return this;
  }


  public updatePedido(): Observable <IOpcionesfacturas> {

    const service_url = ApiHelper.Instance.api_base_url + `user/${this.opcion_id}`;
    const ctx   =   this.getContext();

    const _headers = new Headers ({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + ctx.token
    });

    const _options = new RequestOptions( { headers: _headers } );

    const _params = this.opcion_data;

    return this.http.put( service_url, _params, _options)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server error'));

  }

}
