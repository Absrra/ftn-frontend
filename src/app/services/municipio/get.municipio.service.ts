import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { ApiHelper } from '../../helpers/api.helper';
import { StorageHelper } from '../../helpers/storage.helper';
import { IMunicipio } from '../../models/minicipio.model';


@Injectable()
export class GetMunicipioService extends StorageHelper {
    private municipio_id: string;

    constructor(private http: Http) { super(); }

    public init(municipio_id_: string): GetMunicipioService {
        this.municipio_id = municipio_id_;
        return this;
    }

    public getData(): Observable<IMunicipio> {

        const service_url = ApiHelper.Instance.api_base_url + `user/` + this.municipio_id;
        const ctx   =   this.getContext();

        const _headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + ctx.token
        });

        const _options = new RequestOptions({ headers: _headers });

        return this.http.get(service_url, _options)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));

    }
}
