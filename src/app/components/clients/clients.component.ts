import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { AddUserService } from '../../services/user/add.user.service';
import { GetAllUserService } from '../../services/user/get-all.user.service';

import { SnackService } from '../../helpers/snack.helper';
import { IUser, User } from '../../models/user.model';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
  providers: [
    SnackService,
    GetAllUserService,
    AddUserService
  ]
})
export class ClientsComponent implements OnInit {

  public closeResult: string;
  public users_list: any;
  public new_user: IUser;

  public form_add_user: FormGroup;

  constructor(
    public form: FormBuilder,
    private Snack: SnackService,
    private modalService: NgbModal,
    private AddUser: AddUserService,
    private GetAllUser: GetAllUserService
  ) {

    this.form_add_user = form.group({
      rut: new FormControl('',
        Validators.compose([
          Validators.required
        ])
      ),
      first_name: new FormControl('',
        Validators.compose([
          Validators.required
        ])
      ),
      last_name: new FormControl('',
        Validators.compose([
        ])
      ),
      email: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$')
        ])
      ),
      password: new FormControl('',
        Validators.compose([
          Validators.required
        ])
      ),
      user: '',
      phone: ''
    });
  }

  ngOnInit() {

    //this.getUsers();
  }

  public open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  /**
   * getUsers
   */
  public getUsers() {
    this.GetAllUser.getData().subscribe((response) => {
      this.users_list = response;
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * addUser
   */
  public addUser() {
    this.parse();

    this.AddUser.init(this.new_user).createUser().subscribe((response: any)  => {
      console.log(response);
      if (response.code === 200) {
        this.Snack.showSnackBar('Usuario agregado correctamente');
        this.form_add_user.reset();
        this.getUsers();
      }
    }, (error) => {
      console.log(error);
      this.form_add_user.reset();
    });
  }

  private parse() {
    this.new_user = new User();

    this.new_user.rut         = this.form_add_user.value.rut;
    this.new_user.first_name  = this.form_add_user.value.first_name;
    this.new_user.last_name   = this.form_add_user.value.last_name;
    this.new_user.email       = this.form_add_user.value.email;
    this.new_user.password    = this.form_add_user.value.password;

    console.log(this.new_user);
  }

}
