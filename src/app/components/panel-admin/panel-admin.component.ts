import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StorageHelper } from '../../helpers/storage.helper';
import { IUser, User } from '../../models/user.model';

import { UpdateUserService } from '../../services/user/update.user.service';
import { LogoutAuthService } from '../../services/auth/logout.auth.service';

declare var $: any;

@Component({
  selector: 'app-panel-admin',
  templateUrl: './panel-admin.component.html',
  styleUrls: ['./panel-admin.component.scss'],
  providers: [
    UpdateUserService,
    LogoutAuthService
  ]
})
export class PanelAdminComponent extends StorageHelper implements OnInit {


  public group_name_open: any;
  public langSelect: string;
  public ctx: any;

  constructor(
    public translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private LogoutAuth: LogoutAuthService,
    private UpdateUser: UpdateUserService
  ) {
    super();
    //if (!this.isLogged()) {
    //  this.router.navigate(['/auth/login']);
    //}
  }

  ngOnInit() {
    this.ctx = this.getContext();



    //console.log('panel: ', this.ctx.token);
    $('[data-toggle="popover"]').popover();
    //this.langSelect = this.ctx.user.lang;
    this.translate.setDefaultLang("es");
    this.translate.use("es");


  }



  isOpen(menu_id_: string): boolean {
    if (this.group_name_open === menu_id_) {
        return true;
    }
    return false;
  }

  openMenuGroup(menu_id_: string): void {
    if (this.group_name_open === menu_id_) {
        this.group_name_open = null;
    }else {
        this.group_name_open = menu_id_;
    }
  }

  /**
   * changeLang
   */
  public changeLang(lang: string) {
    this.translate.use(lang);
    this.langSelect = lang;
    //this.ctx.user.lang = lang;
    //console.log('storage: ', this.ctx.user);
    //this.updateUser();
  }

  /**
   * updateUser
   */
  public updateUser() {
    const languaje: IUser = new User();
    console.log(this.langSelect);
    languaje.lang = this.langSelect;
    this.UpdateUser.init(this.ctx.user.id, languaje).updateUser().subscribe((response) => {
      console.log(response);
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * logout
   */
  public logout() {
    //this.LogoutAuth.logout().subscribe((response) => {
    //  console.log(response);
    //  this.delContext();
    //  this.router.navigate(['/auth/login']);
    //  console.log('Salida segura');
    //}, (error) => {
    //  console.log(error);
    //});

    this.delContext();
    this.router.navigate(['/auth/login']);

  }

}
