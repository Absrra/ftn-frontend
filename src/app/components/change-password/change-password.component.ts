import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { ChangePasswordAuthService } from '../../services/auth/change-password.auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  providers: [
    ChangePasswordAuthService
  ]
})
export class ChangePasswordComponent implements OnInit {

  public params: any;
  public form_change_pass: FormGroup;
  public token: string;
  public email: string;
  public is_change: boolean;

  constructor(
    public form: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private ChangePasswordAuth: ChangePasswordAuthService
  ) {

    this.form_change_pass = form.group({
      password: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(5)
        ])
      )
    });
  }

  ngOnInit() {

    this.is_change = false;

    this.params = this.route.params.subscribe(params => {
      this.email  = params['email'];
      this.token  = params['token'];
    });

  }

  /**
   * changePass
   */
  public changePass() {
    const password  = this.form_change_pass.value.password;
    this.ChangePasswordAuth.init(this.email, password, this.token).changePass().subscribe((response) => {
      if (response.code === 200) {
        this.is_change = true;
      }
    }, (error) => {
      console.log(error);
    });
  }

}
