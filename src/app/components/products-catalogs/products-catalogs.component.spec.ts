import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsCatalogsComponent } from './products-catalogs.component';

describe('ProductsCatalogsComponent', () => {
  let component: ProductsCatalogsComponent;
  let fixture: ComponentFixture<ProductsCatalogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsCatalogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsCatalogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
