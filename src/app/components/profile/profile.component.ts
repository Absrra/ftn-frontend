import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UpdateUserService} from '../../services/user/update.user.service';

import { StorageHelper } from '../../helpers/storage.helper';
import { SnackService } from '../../helpers/snack.helper';
import { User, IUser } from '../../models/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [
    UpdateUserService,
    SnackService
  ]
})
export class ProfileComponent extends StorageHelper implements OnInit {

  public ctx: any;
  public form_profile: FormGroup;
  public data_profile: IUser;

  constructor(
    public form: FormBuilder,
    private Snack: SnackService,
    private UpdateUser: UpdateUserService
  ) {
    super();

    this.form_profile = form.group({
      rut: '',
      first_name: '',
      last_name: '',
      username: '',
      email: '',
      phone: '',
      password: ''

    });
   }

  ngOnInit() {
    //this.ctx = this.getContext();
    this.data_profile = new User();
  }

  /**
   * putUser
   */
  public putUser() {
    //this.parse();
    //this.UpdateUser.init(this.ctx.user.id, this.data_profile).updateUser().subscribe((response: any) => {
    //  console.log(response);
    //  if (response.code === 200 ) {
    //    this.Snack.showSnackBar('Datos actualizados correctamente');
    //  }
    //}, (error) => {
    //  console.log(error);
    //});
  }

  private parse() {

    this.data_profile.rut           = this.form_profile.value.rut;
    this.data_profile.first_name    = this.form_profile.value.first_name;
    this.data_profile.last_name     = this.form_profile.value.last_name;
    this.data_profile.username      = this.form_profile.value.username;
    this.data_profile.email         = this.form_profile.value.email;
    if (this.data_profile.password !== undefined ) {
      this.data_profile.password  = this.form_profile.value.password;

    }
    console.log(this.data_profile);
  }

}
